package pl.codementors.zoo;

import java.sql.*;
import java.util.regex.Pattern;

/**
 * Created by psysiu on 6/27/17.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to the ZOO!");

        try (Connection connection = createConnection();) {
            System.out.println("--ANIMALS--");
            showAllAnimalsNames(connection);

            System.out.println("\n--BREEDERS--");
            showAllBreedersNamesAndSurnames(connection);

            System.out.println("\n--ENCLOSURES--");
            showAllEnclosuresNames(connection);

            System.out.println("\n--ANIMALS IN ENCLOSURES--");
            showAllAnimalsInEnclosures(connection);

            System.out.println("\n--ANIMALS AND BREEDERS--");
            showAllAnimalsAndBreeders(connection);

            System.out.println("\n--ANIMALS IN ENCLOSURES AND BREEDERS--");
            showAllAnimalsInEnclosuresAndBreeders(connection);

            System.out.println("\n--ANIMALS COUNT--");
            showAnimalsCount(connection);

            System.out.println("\n--MALES COUNT--");
            showMalesCount(connection);

            System.out.println("\n--WOLVES COUNT--");
            showWolvesCount(connection);

            System.out.println("\n--FEMALES COUNT--");
            showCountByGender(connection, "female");

            System.out.println("\n--TIGERS COUNT--");
            showCountBySpecies(connection, "tiger");

            System.out.println("\n--SPECIES--");
            showAllSpeciesNames(connection);

            System.out.println("\n--SPECIES AFTER ADDING--");
            insertIntoSpecies(connection, "wolverine");
            insertIntoSpecies(connection, "dingo");
            showAllSpeciesNames(connection);

            System.out.println("\n--BREEDERS AFTER ADDING--");
            insertIntoBreeders(connection, "Trashy", "Lowlife");
            showAllBreedersNamesAndSurnames(connection);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }


    public static void showAllAnimalsNames2(Connection connection) throws SQLException {
        String sql = "SELECT name FROM animals";
        Statement statement = null;
        ResultSet result = null;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(sql);
            while (result.next()) {
                String name = result.getString("name");
                System.out.println(name);
            }

        } catch (SQLException ex) {
            throw  ex;
        } finally {
            close(result);
            close(statement);
        }
    }

    public static void close(AutoCloseable close) {
        if (close != null) {
            try {
                close.close();
            } catch (Exception ex) {

            }
        }
    }

    public static void showAllAnimalsNames(Connection connection) throws SQLException {
        String sql = "SELECT name FROM animals";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            while (result.next()) {
                String name = result.getString("name");
                System.out.println(name);
            }
        }
    }

    public static void showAllBreedersNamesAndSurnames(Connection connection) throws SQLException {
        String sql = "SELECT name, surname FROM breeders";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            while (result.next()) {
                String name = result.getString("name");
                String surname = result.getString("surname");
                System.out.println(name + " " + surname);
            }
        }
    }

    public static void showAllEnclosuresNames(Connection connection) throws SQLException {
        String sql = "SELECT name FROM enclosures";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            while (result.next()) {
                String name = result.getString("name");
                System.out.println(name);
            }
        }
    }

    public static void showAllAnimalsInEnclosures(Connection connection) throws SQLException {
        String sql = "SELECT animals.name, enclosures.name FROM animals" +
                " INNER JOIN enclosures ON animals.enclosure = enclosures.id";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            while (result.next()) {
                String animal = result.getString("animals.name");
                String enclosure = result.getString("enclosures.name");
                System.out.println(animal + " in " + enclosure);
            }
        }
    }

    public static void showAllAnimalsAndBreeders(Connection connection) throws SQLException {
        String sql = "SELECT animals.name, breeders.name, breeders.surname FROM animals" +
                " INNER JOIN breeders ON animals.breeder = breeders.id";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            while (result.next()) {
                String animal = result.getString("animals.name");
                String brederName = result.getString("breeders.name");
                String brederSurname = result.getString("breeders.surname");
                System.out.println(brederName + " " + brederSurname + " is taking care of " + animal);
            }
        }
    }

    public static void showAllAnimalsInEnclosuresAndBreeders(Connection connection) throws SQLException {
        String sql = "SELECT animals.name, breeders.name, breeders.surname, enclosures.name FROM animals" +
                " INNER JOIN breeders ON animals.breeder = breeders.id" +
                " INNER JOIN enclosures ON animals.enclosure = enclosures.id";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            while (result.next()) {
                String animal = result.getString("animals.name");
                String brederName = result.getString("breeders.name");
                String brederSurname = result.getString("breeders.surname");
                String enclosure = result.getString("enclosures.name");
                System.out.println(brederName + " " + brederSurname + " is taking care of " + animal + " in " + enclosure);
            }
        }
    }

    public static void showAnimalsCount(Connection connection) throws SQLException {
        String sql = "SELECT COUNT(*) FROM animals";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            result.next();
            int count = result.getInt(1);
            System.out.println("animals count: " + count);
        }
    }

    public static void showMalesCount(Connection connection) throws SQLException {
        String sql = "SELECT COUNT(*) FROM animals WHERE gender = 'male'";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            result.next();
            int count = result.getInt(1);
            System.out.println("animals males count: " + count);
        }
    }

    public static void showWolvesCount(Connection connection) throws SQLException {
        String sql = "SELECT COUNT(*) FROM animals WHERE" +
                " species = (SELECT id FROM species where name = 'wolf')";
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql);) {
            result.next();
            int count = result.getInt(1);
            System.out.println("wolves count: " + count);
        }
    }

    public static void showCountByGender(Connection connection, String gender) throws SQLException {
        String sql = "SELECT COUNT(*) FROM animals WHERE gender = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setString(1, gender);
            try (ResultSet result = statement.executeQuery()) {
                result.next();
                int count = result.getInt(1);
                System.out.println(gender + " count: " + count);
            }
        }
    }

    public static void showCountBySpecies(Connection connection, String species) throws SQLException {
        String sql = "SELECT COUNT(*) FROM animals WHERE" +
                " species = (SELECT id FROM species where name = ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setString(1, species);
            try (ResultSet result = statement.executeQuery()) {
                result.next();
                int count = result.getInt(1);
                System.out.println(species + " count: " + count);
            }
        }
    }

    public static void showAllSpeciesNames(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery("SELECT name FROM species");) {
            while (result.next()) {
                String name = result.getString("name");
                System.out.println(name);
            }
        }
    }

    public static void insertIntoSpecies(Connection connection, String species) throws SQLException {
        String count = "SELECT COUNT(*) FROM species WHERE name = ?";
        String insert = "INSERT INTO species (name) VALUES (?)";
        try (PreparedStatement countStatement = connection.prepareStatement(count);
             PreparedStatement insertStatement = connection.prepareStatement(insert);) {
            countStatement.setString(1, species);
            try (ResultSet result = countStatement.executeQuery();) {
                result.next();
                if (result.getInt(1) == 0) {
                    insertStatement.setString(1, species);
                    insertStatement.execute();
                }
            }
        }
    }

    public static void insertIntoBreeders(Connection connection, String name, String surname) throws SQLException {
        String count = "SELECT COUNT(*) FROM breeders WHERE name = ? AND surname = ?";
        String insert = "INSERT INTO breeders (name, surname) VALUES (?, ?)";
        try (PreparedStatement countStatement = connection.prepareStatement(count);
             PreparedStatement insertStatement = connection.prepareStatement(insert);) {
            countStatement.setString(1, name);
            countStatement.setString(2, surname);
            try (ResultSet result = countStatement.executeQuery();) {
                result.next();
                if (result.getInt(1) == 0) {
                    insertStatement.setString(1, name);
                    insertStatement.setString(2, surname);
                    insertStatement.execute();
                }
            }
        }
    }

    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/mini_zoo"
                        + "?useUnicode=true"
                        + "&useJDBCCompliantTimezoneShift=true"
                        + "&useLegacyDatetimeCode=false"
                        + "&serverTimezone=UTC",
                "zoo_keeper",
                "k33p3r");
    }

}
