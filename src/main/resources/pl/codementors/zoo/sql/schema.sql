DROP DATABASE IF EXISTS mini_zoo;

DROP USER IF EXISTS 'zoo_keeper';

CREATE USER 'zoo_keeper' IDENTIFIED BY 'k33p3r';

CREATE DATABASE mini_zoo CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON mini_zoo.* TO 'zoo_keeper';

USE mini_zoo;

CREATE TABLE breeders (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    surname VARCHAR(256),
    position ENUM('junior', 'senior'),
    PRIMARY KEY (id)
);

CREATE TABLE species (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256) UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE enclosures (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    length INT,
    width INT,
    species INT,
    PRIMARY KEY (id),
    FOREIGN KEY (species) REFERENCES species (id)
);

CREATE TABLE animals (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    age INT,
    gender ENUM('male', 'female'),
    breeder INT,
    species INT,
    enclosure INT,
    PRIMARY KEY (id),
    FOREIGN KEY (breeder) REFERENCES breeders (id),
    FOREIGN KEY (species) REFERENCES species (id),
    FOREIGN KEY (enclosure) REFERENCES enclosures (id)
);

INSERT INTO breeders (name, surname, position) VALUES 
    ('Dmitri', 'Winogradoff', 'senior'),
    ('Kazimir', 'Litvinov', 'junior'),
    ('Ranka', 'Litvinskii', 'junior');

INSERT INTO species (name) VALUES
    ('wolf'),
    ('lynx'),
    ('tiger');

INSERT INTO enclosures (name, length, width, species) VALUES 
    ('canis enclosure', '100', '100', (SELECT id FROM species WHERE name = 'wolf')),
    ('tiger enclosure', '200', '100', (SELECT id FROM species WHERE name = 'tiger')),
    ('cat enclosure', '200', '100', (SELECT id FROM species WHERE name = 'lynx'));
    
INSERT INTO animals (name, age, gender, breeder, species, enclosure) VALUES
    ('Feesrir', 3, 'male', (SELECT id FROM breeders WHERE name = 'Dmitri'),
                           (SELECT id FROM species WHERE name = 'wolf'),
                           (SELECT id FROM enclosures WHERE name = 'canis enclosure')),
    ('Makha', 1, 'female', (SELECT id FROM breeders WHERE name = 'Dmitri'),
                           (SELECT id FROM species WHERE name = 'wolf'),
                           (SELECT id FROM enclosures WHERE name = 'canis enclosure')),
    ('Zrizard', 1, 'male', (SELECT id FROM breeders WHERE name = 'Kazimir'),
                           (SELECT id FROM species WHERE name = 'lynx'),
                           (SELECT id FROM enclosures WHERE name = 'cat enclosure')),
    ('Kuzil', 4, 'male', (SELECT id FROM breeders WHERE name = 'Kazimir'),
                         (SELECT id FROM species WHERE name = 'lynx'),
                         (SELECT id FROM enclosures WHERE name = 'cat enclosure')),
    ('Limriast', 6, 'male', (SELECT id FROM breeders WHERE name = 'Ranka'),
                            (SELECT id FROM species WHERE name = 'tiger'),
                            (SELECT id FROM enclosures WHERE name = 'tiger enclosure')),
    ('Dhaka', 2, 'female', (SELECT id FROM breeders WHERE name = 'Ranka'),
                           (SELECT id FROM species WHERE name = 'tiger'),
                           (SELECT id FROM enclosures WHERE name = 'tiger enclosure'));

ALTER TABLE animals ADD birth_date timestamp;
