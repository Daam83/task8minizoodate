package pl.codementors.zoo.menu;

import pl.codementors.zoo.database.EnclosuresDAO;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Enclosure;
import pl.codementors.zoo.model.Species;

import java.util.List;
import java.util.Scanner;

/**
 * Created by psysiu on 6/29/17.
 */
public class EnclosuresManager extends BaseManager<Enclosure, EnclosuresDAO> {

    public EnclosuresManager() {
        dao = new EnclosuresDAO();
    }

    @Override
    protected Enclosure parseNew(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new width: ");
        int width = scanner.nextInt();
        System.out.print("new height: ");
        int height = scanner.nextInt();
        System.out.print("new species name: ");
        String speciesName = scanner.next();
        Species species = new SpeciesDAO().findByName(speciesName);
        return new Enclosure(name, width, height, species);
    }

    @Override
    protected void copyId(Enclosure from, Enclosure to) {
        to.setId(from.getId());
    }

}
