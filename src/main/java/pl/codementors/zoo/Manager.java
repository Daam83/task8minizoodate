package pl.codementors.zoo;

import pl.codementors.zoo.menu.AnimalsManager;
import pl.codementors.zoo.menu.BreedersManager;
import pl.codementors.zoo.menu.EnclosuresManager;
import pl.codementors.zoo.menu.SpeciesManager;

import java.util.Scanner;

/**
 * Created by psysiu on 6/28/17.
 */
public class Manager {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String command;
        SpeciesManager speciesManager = new SpeciesManager();
        BreedersManager breedersManager = new BreedersManager();
        EnclosuresManager enclosuresManager = new EnclosuresManager();
        AnimalsManager animalsManager = new AnimalsManager();
        boolean run = true;

        while (run) {
            System.out.print("command: ");
            command = scanner.next();

            switch (command) {
                case "species": {
                    speciesManager.manage(scanner);
                    break;
                }
                case "breeders": {
                    breedersManager.manage(scanner);
                    break;
                }
                case "enclosures": {
                    enclosuresManager.manage(scanner);
                    break;
                }
                case "animals": {
                    animalsManager.manage(scanner);
                    break;
                }
                case "quit": {
                    run = false;
                    break;
                }
            }
        }
    }

}
